//
//  CellUpdateDelegate.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 4/2/15.
//  Copyright (c) 2015 MORPCA. All rights reserved.
//

protocol CellUpdateDelegate {
    func didFinishEnteringNewCar()
}
