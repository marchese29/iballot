//
//  CarEntryTableViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/24/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

import UIKit
import CoreData

class CarEntryTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var context: NSManagedObjectContext?
    var competition: Competition!
    
    var hasSelectedCar: Bool = false
    var selectedCar: Car?
    
    var sortedCars: [Car] = []
    var csvFile: NSURL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure the tableView.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        // Only load cars if we have new ones.
        if let url = self.csvFile {
            let hud: MBProgressHUD = MBProgressHUD.showHUDAddedTo(self.tableView, animated: true)
            hud.labelText = "Processing"
            hud.detailsLabelText = "Your CSV File is being processed."
            
            // Load the CSV on a background thread so as not to block up the main one.
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)) {
                // Load the CSV File
                self.loadCSV(url)
                
                // Remove the progress overlay and show the loaded data.
                dispatch_async(dispatch_get_main_queue()) {
                    hud.hide(true)
                    self.tableView.reloadData()
                }
            }
        } else {
            // We already have a competition.
            self.sortedCars = self.competition.cars.sortedArrayUsingDescriptors([NSSortDescriptor(key: "number", ascending: true)]) as! [Car]
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.sortedCars = self.competition.cars.sortedArrayUsingDescriptors([NSSortDescriptor(key: "number", ascending: true)]) as! [Car]
        self.tableView.reloadData()
    }
    
    func loadCSV(url: NSURL) {
        var error: NSError? = nil
        let csv: CSV?
        do {
            csv = try CSV(contentsOfURL: url)
        } catch let error1 as NSError {
            error = error1
            csv = nil
        }
        // We shouldn't be getting parser errors here, but just in case...
        if let err = error {
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                let alert = UIAlertController(
                    title: "Invalid CSV",
                    message: "The provided CSV data was invalid.",
                    preferredStyle: .Alert)
                let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        } else if csv == nil {
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                let alert = UIAlertController(
                    title: "Invalid CSV",
                    message: "The provided CSV data was invalid.",
                    preferredStyle: .Alert)
                let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        } else {
            for row: [String: String] in csv!.rows {
                // Create the new car object.
                let newCar: Car = NSEntityDescription.insertNewObjectForEntityForName("Car", inManagedObjectContext: self.context!) as! Car
                
                // Fill in the data fields.
                newCar.number = Int(row["Car Number"]!)
                newCar.firstName = row["First Name"]
                newCar.lastName = row["Last Name"]
                newCar.year = Int(row["Car Year"]!)
                newCar.model = row["Car Model"]
                newCar.color = row["Car Color"]
                
                // Add the competition to core data.
                self.competition?.mutableSetValueForKey("cars").addObject(newCar)
                newCar.competition = self.competition!
            }
            
            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "number", ascending: true)
            self.sortedCars = self.competition!.cars.sortedArrayUsingDescriptors([descriptor]) as! [Car]
        }
    }

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return self.sortedCars.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("reuseableCell")
        if cell == nil {
            cell = UITableViewCell(style: .Subtitle, reuseIdentifier: "reusableCell")
        }
        
        // Retrieve the instance that this is referring to.
        let car: Car = self.sortedCars[indexPath.row]
        
        cell!.textLabel?.text = "\(car.number) - \(car.lastName), \(car.firstName)"
        
        cell!.detailTextLabel?.text = "\(car.color) \(car.model), \(car.year)"
        cell!.accessoryType = .DisclosureIndicator
        
        return cell!
    }
    
    // MARK: - Events
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // First: deselect the row.
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        // Get the row that was selected.
        let cars: NSArray = self.competition!.cars.sortedArrayUsingDescriptors([NSSortDescriptor(key: "number", ascending: true)])
        self.selectedCar = cars.objectAtIndex(indexPath.row) as? Car
        self.hasSelectedCar = true
        
        self.performSegueWithIdentifier("showNewCarForm", sender: self)
    }
    
    @IBAction func onDoneSelected(sender: AnyObject) {
        do {
            try self.context!.save()
            self.dismissViewControllerAnimated(true, completion: nil)
        } catch {
            print("\(error)")
            let alert: UIAlertController = UIAlertController(
                title: "Error Saving",
                message: "There was an error saving the competition data.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func onAddSelected(sender: AnyObject) {
        self.performSegueWithIdentifier("showNewCarForm", sender: self)
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showNewCarForm" {
            let navController: UINavigationController = segue.destinationViewController as! UINavigationController
            let controller: NewCarViewController = navController.topViewController as! NewCarViewController
            controller.context = self.context
            if let car = self.selectedCar {
                if hasSelectedCar {
                    controller.car = car
                    hasSelectedCar = false
                }
            }
            controller.competition = self.competition
        }
    }
}
