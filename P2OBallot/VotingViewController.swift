//
//  VotingViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 1/31/15.
//  Copyright (c) 2015 MORPCA. All rights reserved.
//

import UIKit
import CoreData

class VotingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CellUpdateDelegate {

    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    // CoreData Context
    var context: NSManagedObjectContext!
    var selectedIndex: Int = -1
    var competition: Competition!
    var isDone: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure the custom cell.
        let nib: UINib = UINib(nibName: "CustomVotingTableViewCell", bundle: nil)
        self.tableView.registerNib(nib, forCellReuseIdentifier: "customVotingCell")
        
        // Set up the core data context.
        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.context = appDelegate.managedObjectContext
        
        // Retrieve the active competition
        let description: NSEntityDescription = NSEntityDescription.entityForName("Competition", inManagedObjectContext: self.context!)!
        let request: NSFetchRequest = NSFetchRequest()
        request.entity = description
        let predicate: NSPredicate = NSPredicate(format: "isActive == %@", NSNumber(bool: true))
        request.predicate = predicate
        let array: NSArray = try! self.context!.executeFetchRequest(request)
//        if let err = error {
//            showInvalidContextDialog(err)
//        } else
        if array.count != 1 {
            showInvalidContextDialog(nil)
        } else {
            self.competition = array.firstObject as! Competition
            self.title = self.competition.name
        }
        
        // Configure the TableView data source.
        if let comp = self.competition {
            self.tableView.dataSource = self
            self.tableView.delegate = self
            self.tableView.allowsMultipleSelection = false
        } else {
            showInvalidContextDialog(nil)
        }
    }
    
    func showInvalidContextDialog(error: NSError?) {
        let message: String = { () -> String in
            if let err = error {
                return "The app failed to create a CoreData context, your votes may not be counted."
            } else {
                return "The app has an invalid number of active competitions, please notify an organizer."
            }
        }()
        
        let alert: UIAlertController = UIAlertController(
            title: "Invalid Context",
            message: message,
            preferredStyle: .Alert)
        let action = UIAlertAction(title: "Okay", style: .Default, handler: { (action: UIAlertAction) -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        })
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func onDoneSelected(sender: AnyObject) {
        let cells: [CustomVotingTableViewCell] = self.tableView.visibleCells as! [CustomVotingTableViewCell]
        let filtered: [CustomVotingTableViewCell] = cells.filter { !$0.isValid }
        if filtered.count > 0 {
            let alert = UIAlertController(
                title: "Invalid Votes",
                message: "There are invalid votes on your ballot.  Please check all of the red votes and make sure the car number is valid.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(
                title: "Are You Finished Voting?",
                message: "Only select okay if you have placed all of the votes you would like to, this action cannot be undone.",
                preferredStyle: .Alert)
            let okay = UIAlertAction(title: "Submit Now", style: .Default, handler: { (action: UIAlertAction) -> Void in
                // The user is done, save everything and head back to the home page.
                
                // Create the new votes as necessary.
                let cars: [Car] = self.competition.cars.sortedArrayUsingDescriptors([NSSortDescriptor(key: "number", ascending: true)]) as! [Car]
                for cell in cells {
                    if let carNumber = cell.getCarNumber() {
                        let newVote: Vote = NSEntityDescription.insertNewObjectForEntityForName("Vote", inManagedObjectContext: self.context) as! Vote

                        // Add the vote to the competition.
                        newVote.competition = self.competition
                        self.competition.mutableSetValueForKey("votes").addObject(newVote)
                    
                        // Add the vote to the car.
                        let car: Car = cars.filter({ $0.number == carNumber }).first!
                        newVote.car = car
                        car.mutableSetValueForKey("votes").addObject(newVote)
                    }
                }
                
                // Save the changes.
                var error: NSError?
                let success: Bool
                do {
                    try self.context.save()
                    success = true
                } catch let error1 as NSError {
                    error = error1
                    success = false
                } catch {
                    fatalError()
                }
                if let err = error {
                    let alert: UIAlertController = UIAlertController(
                        title: "Error",
                        message: "There was an error saving your votes.  We are not sure if they have been properly saved. \"\(err.localizedDescription)\"",
                        preferredStyle: .Alert)
                    let action: UIAlertAction = UIAlertAction(title: "Okay", style: .Default, handler: { (action: UIAlertAction) -> Void in
                        self.context.rollback()
                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                    alert.addAction(action)
                    self.presentViewController(alert, animated: true, completion: nil)
                } else if !success {
                    let alert: UIAlertController = UIAlertController(
                        title: "Error",
                        message: "There was an error saving your votes. Please enter them again.",
                        preferredStyle: .Alert)
                    let action: UIAlertAction = UIAlertAction(title: "Okay", style: .Default, handler: { (action: UIAlertAction) -> Void in
                        self.context.rollback()
                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                    alert.addAction(action)
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(
                        title: "Thank You!",
                        message: "Your votes have been tallied.",
                        preferredStyle: .Alert)
                    let okay = UIAlertAction(title: "Okay", style: .Default, handler: { (action: UIAlertAction) -> Void in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                    alert.addAction(okay)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            })
            let goBack = UIAlertAction(title: "Go Back", style: .Default, handler: nil)
            alert.addAction(okay)
            alert.addAction(goBack)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func onCancelSelected(sender: AnyObject) {
        let controller: UIAlertController = UIAlertController(
            title: "Are you sure you want to cancel?",
            message: "If you cancel, your votes will not be counted.",
            preferredStyle: .Alert)
        let okay: UIAlertAction = UIAlertAction(title: "Okay", style: .Destructive, handler: { (action: UIAlertAction) -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        })
        let cancel: UIAlertAction = UIAlertAction(title: "Cancel", style: .Default, handler: nil)
        controller.addAction(okay)
        controller.addAction(cancel)
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: CustomVotingTableViewCell = self.tableView.dequeueReusableCellWithIdentifier("customVotingCell") as! CustomVotingTableViewCell
        cell.configureCell(voteNumber: indexPath.row, withCompetition: self.competition)
        cell.delegate = self
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60.0
    }
    
    // MARK: - CellUpdateDelegate
    
    func didFinishEnteringNewCar() {
        let allCells: [CustomVotingTableViewCell] = self.tableView.visibleCells as! [CustomVotingTableViewCell]
        // Reset all validations to pre-dupe-check state.
        for cell in allCells {
            cell.validateAndUpdate()
        }
        
        // Check for duplicates.
        for cell in allCells {
            if let number = cell.getCarNumber() {
                for ce in allCells {
                    if let num = ce.getCarNumber() {
                        if ce != cell {
                            // Both cells have cars, and aren't the same cell.
                            let pred = NSPredicate(format: "number == %@", argumentArray: [number])
                            let results: NSSet = self.competition.cars.filteredSetUsingPredicate(pred)
                            
                            if number == num && results.count > 0 {
                                cell.setAsInvalid()
                                cell.carTitleLabel.text = "DUPLICATE CAR NUMBERS"
                            }
                        }
                    }
                }
            }
        }
    }
}
