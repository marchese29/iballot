//
//  LoadContestTableViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 1/8/15.
//  Copyright (c) 2015 MORPCA. All rights reserved.
//

import UIKit
import CoreData

class LoadContestTableViewController: UITableViewController {

    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var context: NSManagedObjectContext!
    var competitions: [Competition]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.context = appDelegate.managedObjectContext
        
        // Retrieve the competitions
        let description: NSEntityDescription = NSEntityDescription.entityForName(
            "Competition", inManagedObjectContext: self.context)!
        let request: NSFetchRequest = NSFetchRequest()
        request.entity = description
        self.competitions = (try! self.context.executeFetchRequest(request)) as! [Competition]
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.competitions.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("competitionReuse")
        if cell == nil {
            cell = UITableViewCell(style: .Subtitle, reuseIdentifier: "competitionReuse")
        }

        let competition: Competition = self.competitions[indexPath.row]
        cell.textLabel?.text = "\(competition.name)"
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "MMM d, yyyy"
        cell.detailTextLabel?.text = "\(competition.location) - \(formatter.stringFromDate(competition.startDate))"
        if competition.isActive == 1 { cell.accessoryType = .Checkmark }
        else { cell.accessoryType = .None }

        return cell
    }

    // MARK: - Events
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        for comp in self.competitions { comp.isActive = 0 }
        self.competitions[indexPath.row].isActive = 1
        self.tableView.reloadData()
    }

    @IBAction func onCancelSelected(sender: AnyObject) {
        self.context.rollback()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onDoneSelected(sender: AnyObject) {
        var error: NSError?
        do {
            try self.context.save()
        } catch let error1 as NSError {
            error = error1
        }
        if let err = error {
            print("\(err)")
        } else {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}
