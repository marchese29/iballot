//
//  extensions.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 3/19/15.
//  Copyright (c) 2015 MORPCA. All rights reserved.
//

import Foundation

// Add the vote count to cars.
extension Car {
    var voteCount: Int { return self.votes.count }
}

// Simple array additions.
extension Array {
    func contains<T : Equatable>(object: T) -> Bool {
        for item in self {
            if (item as! T) == object {
                return true
            }
        }
        return false
    }
}

// Simple string additions.
extension String {
    func find(target: Character, andReplaceWith replace: String) -> String {
        var result = ""
        for char in self.characters {
            if char == target {
                result += replace
            } else {
                result += String(char)
            }
        }
        return result
    }
    
    var length: Int { return self.characters.count }
}
