//
//  NewContestViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/24/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

// NOTE: The contents of this file will not run correctly in the simulator.

import UIKit
import CoreData

class NewContestViewController: FXFormViewController, UIDocumentMenuDelegate, UIDocumentPickerDelegate {
    
    var editMode = false
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var continueButton: UIBarButtonItem!
    
    var context: NSManagedObjectContext?
    var myForm: NewContestForm = NewContestForm()
    var competition: Competition?
    
    var csvFile: NSURL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.formController.form = self.myForm
        
        // Retrieve a reference to the managed object context.
        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.context = appDelegate.managedObjectContext
    }
    
    override func viewWillAppear(animated: Bool) {
        // Handle the case where there's already a competition active.
        if let comp = self.competition {
            self.myForm.competitionName = comp.name
            self.myForm.location = comp.location
            self.myForm.about = comp.about
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showCarEntryTable" {
            let navController: UINavigationController = segue.destinationViewController as! UINavigationController
            let controller: CarEntryTableViewController = navController.topViewController as! CarEntryTableViewController
            controller.context = self.context
            
            if self.competition == nil {
                controller.csvFile = self.csvFile
                
                // De-activate other competitions.
                let description: NSEntityDescription = NSEntityDescription.entityForName(
                    "Competition",
                    inManagedObjectContext: self.context!)!
                let request: NSFetchRequest = NSFetchRequest()
                request.entity = description
                let predicate: NSPredicate = NSPredicate(
                    format: "isActive == %@", NSNumber(bool: true))
                request.predicate = predicate
                let array: NSArray = try! self.context!.executeFetchRequest(request)
                if array.count > 0 {
                    for obj in array {
                        let comp: Competition? = obj as? Competition
                        comp?.isActive = false
                    }
                }
                
                self.competition = NSEntityDescription.insertNewObjectForEntityForName(
                    "Competition",
                    inManagedObjectContext: self.context!) as? Competition
                competition!.startDate = NSDate()
                competition!.isActive = true
            }
            
            competition!.name = myForm.competitionName
            competition!.about = myForm.about
            competition!.location = myForm.location
            
            controller.competition = self.competition
        }
    }
    
    @IBAction func onCancelSelected(sender: AnyObject) {
        self.context?.rollback()
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onContinueSelected(sender: AnyObject) {
        // Validate the data before performing the segue.
        if myForm.competitionName == nil || myForm.competitionName == "" {
            let alert: UIAlertController = UIAlertController(
                title: "Invalid Name",
                message: "Your competition must have a name.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        } else if myForm.location == nil || myForm.location == "" {
            let alert: UIAlertController = UIAlertController(
                title: "No Location",
                message: "The competition location cannot be empty.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        } else if myForm.about == nil || myForm.about == "" {
            let alert: UIAlertController = UIAlertController(
                title: "No Description",
                message: "Your competition must have an about section.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            // Everything is valid, make sure we have a valid CSV File.
            if let url = self.csvFile {
                // We already have the CSV file, perform the segue after validation.
                validateCSV(atURL: url, withSuccessfulCompletion: {
                    self.performSegueWithIdentifier("showCarEntryTable", sender: self)
                })
            } else if !self.editMode {
                // We don't have the CSV file, run the document picker dialog.
                // First check if iCloud is configrued on this device.
                if let token = NSFileManager.defaultManager().ubiquityIdentityToken {
                    // iCloud is available.
                    let importMenu = UIDocumentMenuViewController(documentTypes: ["public.comma-separated-values-text"], inMode: .Import)
                    importMenu.delegate = self
                    importMenu.popoverPresentationController?.barButtonItem = self.continueButton
                    self.presentViewController(importMenu, animated: true, completion: nil)
                } else {
                    // iCloud is not available.
                    let alert: UIAlertController = UIAlertController(
                        title: "iCloud is not Configured",
                        message: "iCloud must be configured if you want to be able to import files.",
                        preferredStyle: .Alert)
                    let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
                    alert.addAction(action)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            } else {
                self.performSegueWithIdentifier("showCarEntryTable", sender: self)
            }
        }
    }
    
    // MARK: - Document picker delegate
    
    func documentMenu(documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        self.presentViewController(documentPicker, animated: true, completion: nil)
    }
    
    func documentMenuWasCancelled(documentMenu: UIDocumentMenuViewController) {
        // Notify the user that we cannot submit without a document.
        let alert: UIAlertController = UIAlertController(
            title: "No File Provided",
            message: "A CSV File must be provided for cars.",
            preferredStyle: .Alert)
        let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func documentPicker(controller: UIDocumentPickerViewController, didPickDocumentAtURL url: NSURL) {
        // Validate the contents of the file.
        validateCSV(atURL: url, withSuccessfulCompletion: { () -> Void in
            self.performSegueWithIdentifier("showCarEntryTable", sender: self)
        })
    }
    
    func displayInvalidCSVAlert(fieldName field: String) {
        let alert: UIAlertController = UIAlertController(
            title: "Missing the '\(field)' field",
            message: "A valid CSV file requires the '\(field)' field to be present.",
            preferredStyle: .Alert)
        let action: UIAlertAction = UIAlertAction(title: "Okay", style: .Default, handler: nil)
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(controller: UIDocumentPickerViewController) {
        // Notify the user that we cannot submit without a document.
        let alert: UIAlertController = UIAlertController(
            title: "No File Provided",
            message: "A CSV File must be provided for cars.",
            preferredStyle: .Alert)
        let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func validateCSV(atURL url: NSURL, withSuccessfulCompletion completion: () -> Void) {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            // Validate that the CSV file has the right data columns.
            var error: NSError? = nil
            let content: CSV?
            do {
                content = try CSV(contentsOfURL: url)
            } catch let error1 as NSError {
                error = error1
                content = nil
            } catch {
                fatalError()
            }
            if let err = error {
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    let alert: UIAlertController = UIAlertController(
                        title: "Invalid CSV",
                        message: "There was an error parsing the provided CSV file. \(err.localizedDescription).",
                        preferredStyle: .Alert)
                    let action: UIAlertAction = UIAlertAction(title: "Okay", style: .Default, handler: nil)
                    alert.addAction(action)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            } else if let csv = content {
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    let headers: [String] = csv.headers
                    
                    if !headers.contains("Car Number") {
                        self.displayInvalidCSVAlert(fieldName: "Car Number")
                    } else if !headers.contains("First Name") {
                        self.displayInvalidCSVAlert(fieldName: "First Name")
                    } else if !headers.contains("Last Name") {
                        self.displayInvalidCSVAlert(fieldName: "Last Name")
                    } else if !headers.contains("Car Model") {
                        self.displayInvalidCSVAlert(fieldName: "Car Model")
                    } else if !headers.contains("Car Year") {
                        self.displayInvalidCSVAlert(fieldName: "Car Year")
                    } else if !headers.contains("Car Color") {
                        self.displayInvalidCSVAlert(fieldName: "Car Color")
                    } else {
                        self.csvFile = url
                        completion()
                    }
                }
            } else {
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    let alert: UIAlertController = UIAlertController(
                        title: "Invalid CSV",
                        message: "There was an error parsing the provided CSV file.",
                        preferredStyle: .Alert)
                    let action: UIAlertAction = UIAlertAction(title: "Okay", style: .Default, handler: nil)
                    alert.addAction(action)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        }
    }
}
