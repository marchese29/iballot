//
//  FullReportTableViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 2/21/15.
//  Copyright (c) 2015 Daniel Marchese. All rights reserved.
//

import UIKit
import CoreData

class FullReportTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext!
    var competition: Competition!
    var cars: [(Car, Int)] = []
    
    @IBOutlet weak var doneButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Retrieve a reference to the managed object context.
        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.context = appDelegate.managedObjectContext
        
        // Retrieve the active competition
        let description: NSEntityDescription = NSEntityDescription.entityForName("Competition", inManagedObjectContext: self.context!)!
        let request: NSFetchRequest = NSFetchRequest()
        request.entity = description
        let predicate: NSPredicate = NSPredicate(format: "isActive == %@", NSNumber(bool: true))
        request.predicate = predicate
        let array: NSArray = try! self.context!.executeFetchRequest(request)
        self.competition = array.firstObject as? Competition
        
        // Get the cars and sort them.
        let descriptor: NSSortDescriptor = NSSortDescriptor(key: "number", ascending: false)
        let sorted: NSArray = self.competition.cars.sortedArrayUsingDescriptors([descriptor])
        var carList: [Car] = []
        for car in sorted { carList.append(car as! Car) }
        carList.sortInPlace { $0.voteCount > $1.voteCount }
        
        // Load up the cars array.
        var placement = 1
        for (i, car) in carList.enumerate() {
            if i == 0 || car.voteCount == carList[i-1].voteCount {
                let item: (Car, Int) = (car, placement)
                self.cars.append(item)
            } else {
                placement += 1
                let item: (Car, Int) = (car, placement)
                self.cars.append(item)
            }
        }
        
        self.tableView.reloadData()
    }
    
    @IBAction func onDoneSelected(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return self.cars.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("reportReuseableCell")
        if cell == nil {
            cell = UITableViewCell(style: .Subtitle, reuseIdentifier: "reportReusableCell")
        }
        
        let (car, placement): (Car, Int) = self.cars[indexPath.row]
        
        if car.lastName != nil && car.firstName != nil && car.lastName != "" && car.firstName != "" {
            cell!.textLabel?.text = "\(indexPath.row). \(getOrdinalForInteger(placement)) (\(car.voteCount) Votes), \(car.number) - \(car.lastName!), \(car.firstName!)"
        } else {
            cell!.textLabel?.text = "\(indexPath.row) \(getOrdinalForInteger(placement)) (\(car.voteCount) Votes), \(car.number)"
        }
        
        if car.year != nil && car.year != 0 && car.model != nil && car.model != "" {
            cell!.detailTextLabel?.text = "\(car.model!), \(car.year!)"
        } else {
            cell!.detailTextLabel?.text = ""
        }
        cell!.accessoryType = .DisclosureIndicator
        
        return cell!
    }

}
