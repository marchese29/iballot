//
//  CustomVotingTableViewCell.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 4/2/15.
//  Copyright (c) 2015 MORPCA. All rights reserved.
//

import UIKit
import Foundation

class CustomVotingTableViewCell: UITableViewCell, UITextFieldDelegate, APNumberPadDelegate {
    
    // Is this cell valid?
    var isValid = true
    
    // Competition for validation purposes.
    var competition: Competition!
    
    // The car if applicable
    var car: Car?
    
    // The delegate for callbacks at the end of validations.
    var delegate: CellUpdateDelegate!

    // Outlets
    @IBOutlet weak var voteNumberLabel: UILabel!
    @IBOutlet weak var carTitleLabel: UILabel!
    @IBOutlet weak var ownerInfoLabel: UILabel!
    @IBOutlet weak var voteEntryField: UITextField!
    var numberGesture: UITapGestureRecognizer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.voteEntryField.delegate = self
        
        // Configure the number pad.
        let numberPad: APNumberPad = APNumberPad(delegate: self)
        numberPad.leftFunctionButton.setTitle("Enter", forState: .Normal)
        numberPad.leftFunctionButton.titleLabel?.adjustsFontSizeToFitWidth = true
        voteEntryField.inputView = numberPad
        
        // Configure the tap gestures on the car number.
        self.numberGesture = UITapGestureRecognizer(target: self, action: #selector(onCarNumberLabelSelected))
        //self.numberGesture = UITapGestureRecognizer(target: self, action: "onCarNumberLabelSelected")
        self.voteNumberLabel.addGestureRecognizer(self.numberGesture)
    }
    
    func getCarNumber() -> NSNumber? {
        return Int(voteEntryField.text!)
    }
    
    func setAsInvalid() {
        self.carTitleLabel.textColor = UIColor.whiteColor()
        self.voteNumberLabel.backgroundColor = UIColor.redColor()
        self.carTitleLabel.backgroundColor = UIColor.redColor()
        self.ownerInfoLabel.backgroundColor = UIColor.redColor()
        self.carTitleLabel.text = "INVALID CAR NUMBER"
        self.ownerInfoLabel.text = ""
        self.isValid = false
    }
    
    func setAsNeutral() {
        self.carTitleLabel.textColor = UIColor.blackColor()
        self.voteNumberLabel.backgroundColor = UIColor.lightGrayColor()
        self.carTitleLabel.backgroundColor = UIColor.lightGrayColor()
        self.carTitleLabel.text = ""
        self.ownerInfoLabel.backgroundColor = UIColor.lightGrayColor()
        self.ownerInfoLabel.text = ""
        self.isValid = true
    }
    
    func setAsValid(car: Car!) {
        // Configure the colors.
        self.carTitleLabel.textColor = UIColor.blackColor()
        self.isValid = true
        self.voteNumberLabel.backgroundColor = UIColor.greenColor()
        self.carTitleLabel.backgroundColor = UIColor.greenColor()
        self.ownerInfoLabel.backgroundColor = UIColor.greenColor()
        
        // Configure the labels.
        self.carTitleLabel.text = "\(car.color) \(car.model), \(car.year)"
        self.ownerInfoLabel.text = "\(car.lastName), \(car.firstName)"
    }
    
    func configureCell(voteNumber number: Int, withCompetition competition: Competition) {
        self.voteNumberLabel.text = "\(number + 1)"
        self.carTitleLabel.text = ""
        self.ownerInfoLabel.text = ""
        self.competition = competition
    }
    
    func validateAndUpdate() -> Car? {
        self.carTitleLabel.text = ""
        self.ownerInfoLabel.text = ""
        if voteEntryField.text != "" {
            if let number: NSNumber = Int(voteEntryField.text!) {
                let pred = NSPredicate(format: "number == %@", argumentArray: [number])
                let results: NSSet = self.competition.cars.filteredSetUsingPredicate(pred)
                if let car = results.anyObject() as? Car {
                    self.setAsValid(car)
                    return car
                } else {
                    self.setAsInvalid()
                }
            } else {
                self.setAsInvalid()
            }
        } else {
            self.setAsNeutral()
        }
        
        return nil
    }
    
    @IBAction func onTextChanged(sender: AnyObject) {
        // Dismiss the keyboard if this is the third character
        if self.voteEntryField.text!.length == 3 {
            self.voteEntryField.endEditing(true)
        }
    }

    @IBAction func onEditingFinished(sender: AnyObject) {
        self.delegate.didFinishEnteringNewCar()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.voteEntryField.placeholder = nil
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.voteEntryField.placeholder = "Tap to Enter Vote"
    }
    
    func onCarNumberLabelSelected() {
        self.voteEntryField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func numberPad(numberPad: APNumberPad!, functionButtonAction functionButton: UIButton!, textInput: UIResponder!) {
        self.voteEntryField.endEditing(true)
    }
}
