//
//  Constants.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/18/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

let DEFAULTS_USERNAME: String = "adminUsername"
let DEFAULTS_PASSWORD: String = "adminPassword"

func getOrdinalForInteger(number: Int) -> String {
    let digit: Int = number % 10
    let tens: Int = (number / 10) % 10
    
    if tens == 1 {
        return "\(number)th"
    } else if digit == 1 {
        return "\(number)st"
    } else if digit == 2 {
        return "\(number)nd"
    } else if digit == 3 {
        return "\(number)rd"
    } else {
        return "\(number)th"
    }
}
