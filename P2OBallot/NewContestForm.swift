//
//  NewContestForm.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/24/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

class NewContestForm: NSObject, FXForm {
    
    var competitionName: String!
    var location: String!
    var about: String!
    
    var startDate: NSDate!
    var endDate: NSDate!
    
    func fields() -> [AnyObject]! {
        return ["competitionName", "location", "about"]
    }
    
    func competitionNameField() -> NSDictionary {
        return NSDictionary(dictionary: [FXFormFieldHeader: NSString(string: "Overview")])
    }
    
    func aboutField() -> NSDictionary {
        return NSDictionary(dictionary: [FXFormFieldType: FXFormFieldTypeLongText])
    }
}
