//
//  NewCarViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/24/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

import UIKit
import CoreData

class NewCarViewController: FXFormViewController {
    
    var editMode = true

    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var context: NSManagedObjectContext?
    var competition: Competition?
    var car: Car?
    
    var myForm: NewCarForm = NewCarForm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.formController.form = myForm
    }
    
    override func viewWillAppear(animated: Bool) {
        
        // If there's an old car, then we show its info.
        if let oldCar = self.car {
            myForm.ownerFirstName = oldCar.firstName
            myForm.ownerLastName = oldCar.lastName
            myForm.model = oldCar.model
            myForm.year = oldCar.year?.stringValue
            myForm.color = oldCar.color
        }
        
        super.viewWillAppear(animated)
    }
    
    func invalidDataAlert(title: String, description: String) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: .Alert)
        let okay = UIAlertAction(title: "Okay", style: .Default, handler: nil)
        alert.addAction(okay)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func onCancelSelected(sender: AnyObject) {
        if !editMode {
            self.context!.deleteObject(self.car!)
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onSaveSelected(sender: AnyObject) {
        // See if we have an existing car.
        if self.car == nil {
            self.car = NSEntityDescription.insertNewObjectForEntityForName("Car", inManagedObjectContext: self.context!) as? Car
            self.car?.competition = self.competition
            self.competition?.mutableSetValueForKey("cars").addObject(self.car!)
            
            // Assign a car number
            let cars: [Car] = self.competition?.cars.sortedArrayUsingDescriptors([NSSortDescriptor(key: "number", ascending: true)]) as! [Car]
            self.car?.number = Int(cars.last!.number) + 1
        }
        
        if myForm.ownerFirstName == nil || myForm.ownerFirstName == "" {
            invalidDataAlert("No First Name", description: "All cars must have owner information.")
        } else if myForm.ownerLastName == nil || myForm.ownerLastName == "" {
            invalidDataAlert("No Last Name", description: "All cars must have owner information.")
        } else if myForm.model == nil || myForm.model == "" {
            invalidDataAlert("No Model", description: "All cars must have a model.")
        } else if myForm.year == nil || myForm.year == "" {
            invalidDataAlert("No Year", description: "All cars must have a year.")
        } else if Int(myForm.year) == nil {
            invalidDataAlert("Invalid Year", description: "The year you provided was invalid.")
        } else if myForm.color == nil || myForm.color == "" {
            invalidDataAlert("No Color", description: "All cars must have a color.")
        } else {
            self.car?.firstName = myForm.ownerFirstName
            self.car?.lastName = myForm.ownerLastName
            self.car?.model = myForm.model
            self.car?.color = myForm.color
            self.car?.year = Int(myForm.year)
            
            let alert = UIAlertController(title: "Car Number: \(self.car!.number)", message: "", preferredStyle: .Alert)
            let okay = UIAlertAction(title: "Okay", style: .Default, handler: { (action: UIAlertAction!) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
            })
            alert.addAction(okay)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
}
