//
//  ReportTableViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 2/15/15.
//  Copyright (c) 2015 MORPCA. All rights reserved.
//

import UIKit
import CoreData

class ReportTableViewController: UITableViewController {

    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    
    @IBOutlet weak var completeResults: UITableViewCell!
    
    @IBOutlet weak var carNumberOne: UILabel!
    @IBOutlet weak var carNumberTwo: UILabel!
    @IBOutlet weak var carNumberThree: UILabel!
    
    @IBOutlet weak var firstPlaceOwner: UILabel!
    @IBOutlet weak var secondPlaceOwner: UILabel!
    @IBOutlet weak var thirdPlaceOwner: UILabel!
    
    @IBOutlet weak var voteTally1: UILabel!
    @IBOutlet weak var voteTally2: UILabel!
    @IBOutlet weak var voteTally3: UILabel!
    
    @IBOutlet weak var voteCount: UILabel!
    
    @IBOutlet var recognizer: UITapGestureRecognizer!
    
    var context: NSManagedObjectContext!
    var competition: Competition?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Retrieve a reference to the managed object context.
        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.context = appDelegate.managedObjectContext
        
        // Retrieve the active competition
        let description: NSEntityDescription = NSEntityDescription.entityForName("Competition", inManagedObjectContext: self.context!)!
        let request: NSFetchRequest = NSFetchRequest()
        request.entity = description
        let predicate: NSPredicate = NSPredicate(format: "isActive == %@", NSNumber(bool: true))
        request.predicate = predicate
        let array: NSArray = try! self.context!.executeFetchRequest(request)
        self.competition = array.firstObject as? Competition
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let comp = self.competition {
            // Set the overall votecount
            self.voteCount.text = "\(comp.votes.count) votes cast"
            
            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "number", ascending: false)
            let sorted: NSArray = comp.cars.sortedArrayUsingDescriptors([descriptor])
            var cars: [Car] = []
            for car in sorted { cars.append(car as! Car) }
            cars.sortInPlace { $0.voteCount > $1.voteCount }
            
            let firstCar: Car = cars[0]
            let secondCar: Car = cars[1]
            let thirdCar: Car = cars[2]
            self.firstLabel.text = "1st place - " + textLabelForCar(firstCar)
            if secondCar.voteCount == firstCar.voteCount {
                self.secondLabel.text = "1st place - " + textLabelForCar(secondCar)
                if thirdCar.voteCount == secondCar.voteCount {
                    self.thirdLabel.text = "1st place - " + textLabelForCar(secondCar)
                } else {
                    self.thirdLabel.text = "2nd place - " + textLabelForCar(thirdCar)
                }
            } else {
                self.secondLabel.text = "2nd place - " + textLabelForCar(secondCar)
                if thirdCar.voteCount == secondCar.voteCount {
                    self.thirdLabel.text = "2nd place - " + textLabelForCar(thirdCar)
                } else {
                    self.thirdLabel.text = "3rd place - " + textLabelForCar(thirdCar)
                }
            }
            configurePlacementCell(firstCar, carNumberLabel: carNumberOne, ownerLabel: firstPlaceOwner, voteTally: voteTally1)
            configurePlacementCell(secondCar, carNumberLabel: carNumberTwo, ownerLabel: secondPlaceOwner, voteTally: voteTally2)
            configurePlacementCell(thirdCar, carNumberLabel: carNumberThree, ownerLabel: thirdPlaceOwner, voteTally: voteTally3)
        } else {
            
        }
    }
    
    func configurePlacementCell(car: Car, carNumberLabel: UILabel!, ownerLabel: UILabel!, voteTally: UILabel!) {
        carNumberLabel.text = "\(car.number)"
        ownerLabel.text = ownerLabelForCar(car)
        voteTally.text = "\(car.voteCount) Votes"
    }
    
    func textLabelForCar(car: Car) -> String {
        if car.year != nil && car.year != 0 && car.model != nil && car.model != "" {
            return "\(car.model!), \(car.year!)"
        } else {
            return ""
        }
    }
    
    func ownerLabelForCar(car: Car) -> String {
        if car.lastName != nil && car.firstName != nil && car.lastName != "" && car.firstName != "" {
            return "\(car.lastName!), \(car.firstName!)"
        } else {
            return ""
        }
    }
    
    @IBAction func onFullResultsSelected(sender: AnyObject) {
        self.performSegueWithIdentifier("showFullResults", sender: self)
    }

}
