//
//  ArchiveViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/19/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

import UIKit
import CoreData

class ArchiveViewController: UIViewController {
    
    @IBOutlet weak var newContestButton: UIButton!
    @IBOutlet weak var editContestButton: UIButton!
    
    var editSelected: Bool = false
    var csvFile: NSURL?
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if let url = self.csvFile {
            self.performSegueWithIdentifier("showNewContestPrompt", sender: self)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showNewContestPrompt" && editSelected {
            let navController: UINavigationController = segue.destinationViewController as! UINavigationController
            let controller: NewContestViewController = navController.topViewController as! NewContestViewController
            self.editSelected = false
            
            // Pull the current active competition.
            let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let context: NSManagedObjectContext! = appDelegate.managedObjectContext
            let request: NSFetchRequest = NSFetchRequest()
            let description: NSEntityDescription! = NSEntityDescription.entityForName("Competition", inManagedObjectContext: context)
            let predicate: NSPredicate! = NSPredicate(format: "isActive == %@", NSNumber(bool: true))
            request.entity = description
            request.predicate = predicate
            let result: NSArray!
            do {
                result = try context.executeFetchRequest(request)
            } catch _ as NSError {
                result = nil
            }
            let competition: Competition = result.firstObject! as! Competition
            
            controller.competition = competition
            controller.editMode = true
        } else if segue.identifier == "showNewContestPrompt" && !editSelected {
            let navController: UINavigationController = segue.destinationViewController as! UINavigationController
            let controller: NewContestViewController = navController.topViewController as! NewContestViewController
            controller.csvFile = self.csvFile
        }
    }
    
    @IBAction func onNewContestSelected(sender: AnyObject) {
        self.performSegueWithIdentifier("showNewContestPrompt", sender: self)
    }
    
    @IBAction func onEditContestSelected(sender: AnyObject) {
        self.editSelected = true
        self.performSegueWithIdentifier("showNewContestPrompt", sender: self)
    }
}
